# LinuxModule
A device driver which permits to read sensor data via I2C bus

Compile :
	make
Install :
	sudo insmod sensor_driver.ko
Create the device :
	sudo mknod /dev/sensor_driver c 90 0
Change the permissions :
	sudo chmod 777 /dev/sensor_driver
Read the device :
	sudo cat /dev/sensor_driver
