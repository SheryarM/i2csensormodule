#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/ioctl.h>
#include <linux/i2c.h>

#define I2C_BUS_AVAILABLE   (          1   )       // I2C bus available in our Raspberry Pi
#define SLAVE_DEVICE_NAME   ( "BMP_DRIVER" )       // device and driver name
#define SLAVE_ADDR          (       0x76   )       // slave address

static struct i2c_adapter *bmp_i2c_adapter = NULL; // I2C adapter structure
static struct i2c_client  *bmp_i2c_client  = NULL; // I2C client structure

/* variables for temperature calculation */
s32 dig_T1, dig_T2, dig_T3;

/* structure that has slave device id */
static const struct i2c_device_id bmp_id[] = {
        { SLAVE_DEVICE_NAME, 0 },
        { }
};
/* I2C driver structure that has to be added to linux */
static struct i2c_driver bmp_driver = {
        .driver = {
		.name   = SLAVE_DEVICE_NAME,
		.owner  = THIS_MODULE,
        },
};
/* I2C board info strucutre */
static struct i2c_board_info bmp_i2c_board_info = {
	I2C_BOARD_INFO(SLAVE_DEVICE_NAME, SLAVE_ADDR)
};

static int dev_open (struct inode *inod, struct file *fil);
static int dev_release (struct inode *inod, struct file *fil);
static ssize_t dev_read (struct file *fil, char *buff, size_t len, loff_t *off);
//static ssize_t dev_write (struct file *fil, const char *buff, size_t len, loff_t *off);
//static long dev_ioctl (struct file *fil, unsigned int cmd, unsigned long arg);
static struct file_operations fops =
{
        .owner = THIS_MODULE,
        .open = dev_open,
        .release = dev_release,
        .read = dev_read,
        //.write = dev_write,
        //.unlocked_ioctl = dev_ioctl,
};
static int dev_open (struct inode *inod, struct file *fil){
        printk(KERN_ALERT "Open \n");
        return 0;
}
static int dev_release (struct inode *inod, struct file *fil){
        printk(KERN_ALERT "Close \n");
        return 0;
}
/**
  * @brief read current temperature from BMP280 sensor
  *
  * @return temperature in degree
 */
s32 read_temperature(void) {
	int var1, var2;
        s32 raw_temp;
        s32 d1, d2, d3;

        /* read temperature */
        d1 = i2c_smbus_read_byte_data(bmp_i2c_client, 0xFA);
        d2 = i2c_smbus_read_byte_data(bmp_i2c_client, 0xFB);
        d3 = i2c_smbus_read_byte_data(bmp_i2c_client, 0xFC);
        raw_temp = ((d1<<16) | (d2<<8) | d3) >> 4;

        /* calculate temperature in degree */
        var1 = ((((raw_temp >> 3) - (dig_T1 << 1))) * (dig_T2)) >> 11;
	
	var2 = (((((raw_temp >> 4) - (dig_T1)) * ((raw_temp >> 4) - (dig_T1))) >> 12) * (dig_T3)) >> 14;
	
	return ((var1 + var2) *5 + 128) >> 8;
}
/**
 * @brief Get data out of buffer
 */
static ssize_t dev_read (struct file *fil, char *buff, size_t len, loff_t *off){
	int to_copy, not_copied, delta;
        char out_string[20] = {'\0'};
        int temperature;

        /* get amount of bytes to copy */
        to_copy = min(sizeof(out_string), len);
        /* get temperature */
        temperature = read_temperature();
        snprintf(out_string, sizeof(out_string), "%d.%d\n", temperature/100, temperature%100);
        //printk(KERN_INFO "%s \n",out_string);
        /* copy data to user */
        not_copied = copy_to_user(buff, out_string, to_copy);
        /* calculate delta */
        delta = to_copy - not_copied;

        return delta;
}
static int __init bmp_driver_init(void){
	u8 id = 0x00;
        int t = register_chrdev(90, "char_device", &fops);
        if(t<0){
		printk(KERN_ALERT "registration failed ! \n");
        }
        else{
		printk(KERN_ALERT "registration success ! \n");
                //timer_setup(&exp_timer, do_smth, 0);
                //mod_timer(&exp_timer, jiffies+2*HZ);
        	bmp_i2c_adapter = i2c_get_adapter(I2C_BUS_AVAILABLE);
		bmp_i2c_client  = i2c_new_client_device(bmp_i2c_adapter, &bmp_i2c_board_info);
		i2c_add_driver(&bmp_driver);
		i2c_put_adapter(bmp_i2c_adapter);
		id = i2c_smbus_read_byte_data(bmp_i2c_client, 0xD0);
		printk(KERN_INFO "id=0x%x \n", id);
		/* read calibration values */
         	dig_T1 = i2c_smbus_read_word_data(bmp_i2c_client, 0x88);
         	dig_T2 = i2c_smbus_read_word_data(bmp_i2c_client, 0x8a);
         	dig_T3 = i2c_smbus_read_word_data(bmp_i2c_client, 0x8c);
		if(dig_T2 > 32767){
                	 dig_T2 -= 65536;
		}
		if(dig_T3 > 32767){
                	 dig_T3 -= 65536;
		}
		/* initialice the sensor */
         	i2c_smbus_write_byte_data(bmp_i2c_client, 0xf5, 5<<5);
         	i2c_smbus_write_byte_data(bmp_i2c_client, 0xf4, ((5<<5) | (5<<2) | (3<<0)));
	}

	return t;
}
static void __exit bmp_driver_exit(void){
	i2c_del_driver(&bmp_driver);
	unregister_chrdev(90, "char_device");
        //del_timer(&exp_timer);
        printk(KERN_ALERT "GoodBye \n");
}
module_init(bmp_driver_init);
module_exit(bmp_driver_exit);
MODULE_AUTHOR("SheryaM");
MODULE_LICENSE("GPL");
MODULE_VERSION("1.0.0");
