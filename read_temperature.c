#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <linux/i2c-dev.h>
#include <i2c/smbus.h>

#define SLAVE_ADDR ( 0x76  )

__s32 read_temperature(int file, __s32 dig_T1, __s32 dig_T2, __s32 dig_T3);

int main(void){
	int file;
	int adapter_nr = 1;
	char filename[20];
	__s32 temperature;
	__u8 id;
	__s32 dig_T1, dig_T2, dig_T3;
	
	snprintf(filename, 19, "/dev/i2c-%d", adapter_nr);
	file = open(filename, O_RDWR);
	if(file < 0){
		printf("error 1");
		exit(1);
	}
	
	if(ioctl(file, I2C_SLAVE, SLAVE_ADDR) < 0){
		printf("error 2");
		exit(1);
	}

	/* initialize the sensor */	
	i2c_smbus_write_byte_data(file, 0xE0, 0xB6);
	id = i2c_smbus_read_byte_data(file, 0xD0);
	printf("0x%x", id); // id should be 0x58
	
	/* read calibration values */
        dig_T1 = i2c_smbus_read_word_data(file, 0x88);
        dig_T2 = i2c_smbus_read_word_data(file, 0x8A);
        dig_T3 = i2c_smbus_read_word_data(file, 0x8C);
	if(dig_T2 > 32767){
		dig_T2 -= 65536;
	}
	if(dig_T3 > 32767){
		dig_T3 -= 65536;
	}
	/* configure the sensor */
        i2c_smbus_write_byte_data(file, 0xF5, 5<<5);
        i2c_smbus_write_byte_data(file, 0xF4, ((5<<5) | (5<<2) | (3<<0)));
	while(1){
		temperature = read_temperature(file, dig_T1, dig_T2, dig_T3);
		printf("\n %d.%d °C", temperature/100, temperature%100);
	}

	return 0;
}

/* read temperature function (formula given in bmp280's datasheet) */
__s32 read_temperature(int file, __s32 dig_T1, __s32 dig_T2, __s32 dig_T3) {
	int var1, var2;
        __s32 raw_temp;
        __s32 d1, d2, d3;
 
        /* read temperature */
        d1 = i2c_smbus_read_byte_data(file, 0xFA);
        d2 = i2c_smbus_read_byte_data(file, 0xFB);
        d3 = i2c_smbus_read_byte_data(file, 0xFC);
        raw_temp = ((d1<<16) | (d2<<8) | d3) >> 4;
 
        /* calculate temperature in degree */
        var1 = ((((raw_temp >> 3) - (dig_T1 << 1))) * (dig_T2)) >> 11;
 
        var2 = (((((raw_temp >> 4) - (dig_T1)) * ((raw_temp >> 4) - (dig_T1))) >> 12) * (dig_T3)) >> 14;
	
	return ((var1 + var2) *5 + 128) >> 8;
}
